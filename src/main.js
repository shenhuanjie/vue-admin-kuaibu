import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'

import './assets/base.css'
import axios from 'axios'

const app = createApp(App)
app.provide('$axios', axios)

app.use(createPinia())
app.use(ElementPlus)
app.use(router)

app.mount('#app')
