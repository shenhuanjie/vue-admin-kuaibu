/**
 * 路由类型-枚举
 */
export default {
  /**
   * 页面
   */
  PAGE: 'page',
  /**
   * 模块
   */
  MODULE: 'module',
  /**
   * 视图
   */
  VIEW: 'view'
}
