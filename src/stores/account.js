import { defineStore } from 'pinia'
// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
export const useAccount = defineStore('account', {
  // state
  state: () => ({
    account: {}
  }),
  //  getters
  getters: {
    /**
     * 判断是否登录
     * @return {boolean}
     */
    isLogin() {
      return !!this.account.token || !!localStorage.getItem('token')
    }
  },
  // actions
  actions: {
    /**
     * 登录
     * @param account
     */
    login(account) {
      this.account = account
      // 本地存储
      localStorage.setItem('token', account.token)
    },
    /**
     * 退出登录
     */
    logout() {
      this.account = {}
      // 清空本地存储
      localStorage.removeItem('token')
    }
  }
})
