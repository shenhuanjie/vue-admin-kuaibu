import { defineStore } from 'pinia'

/**
 * 菜单状态
 */
export const useRouterMenu = defineStore('routerMenu', {
  state: () => ({
    /**
     * 当前菜单
     */
    current: {}
  })
})
